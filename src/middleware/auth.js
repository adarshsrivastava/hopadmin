export default function auth(next) {
    if(!localStorage.getItem('access-token') && localStorage.getItem('access-token') != ''){
        console.log('Invalid request.')
        next({ path: '/login' })
    }else{
        console.log('Valid request.')
        next();
    }
}