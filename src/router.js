import {createRouter, createWebHistory} from 'vue-router'

import auth from '@/middleware/auth'

import Login from '@/pages/Login.vue'
import Dashboard from '@/pages/admin/Dashboard.vue'
import Daily from '@/pages/admin/passes/Daily.vue'
import Upcoming from '@/pages/admin/passes/Upcoming.vue'
import Blocked from '@/pages/admin/passes/Blocked.vue'
import Expired from '@/pages/admin/passes/Expired.vue'
import AddNew from '@/pages/admin/passes/Add.vue'
import UserList from '@/pages/admin/user/ListUser.vue'

const routes = [
    
    { 
      path: '/',
      name : 'dashboard', 
      component: Dashboard,
      meta: { requiresAuth: true }
    },

    { 
      path: '/login',
      name: 'login',
      component: Login
    },

    { 
      path: '/passes/daily-specials', 
      name : 'daily', 
      component: Daily,
      meta: { requiresAuth: true }
    },

    { 
      path: '/passes/upcoming-passes', 
      name : 'upcoming', 
      component: Upcoming,
      meta: { requiresAuth: true }
    },

    { 
      path: '/passes/block-passes', 
      name : 'block', 
      component: Blocked,
      meta: { requiresAuth: true }
    },

    { 
      path: '/passes/expired', 
      name : 'expired', 
      component: Expired,
      meta: { requiresAuth: true }
    },

    { 
      path: '/passes/add-new', 
      name : 'add-new-pass', 
      component: AddNew,
      meta: { requiresAuth: true }
    },

    { 
      path: '/users/list', 
      name : 'user-list', 
      component: UserList,
      meta: { requiresAuth: true }
    }

  ]
 

const router = createRouter({
    history: createWebHistory('/hop-admin/dist/'),
    routes
})

 
router.beforeEach((to, from, next) => {
  if(to.meta.requiresAuth){
    auth(next)
  }else{
    next()
  }
})

export default router