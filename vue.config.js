// vue.config.js

/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
  publicPath: '/hop-admin/dist/',
  devServer: {
    // proxy: 'http://synchsofthq.development:1499'
    proxy: {
      '^/api': {
        target: 'http://dev.synchsoftventures.com:1499',
        ws: true,
        changeOrigin: true
      }
    }
  }
}